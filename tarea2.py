from abc import ABC, abstractmethod
import statistics
import random
import string
import matplotlib.pyplot as plt

class GeneticAlgorithm(ABC):
    def __init__(self, ngenes, nsubjects, solution, selection_method="roulette", verbose=True, n_tournament = 8):
        """ngenes number of genes, nsubjects number of subjects
        solution a solution for when the learning is supervised, selection method can
        be either roulette or tournament, verbose stores fitness data across loops and n_tournament is the 
        amount of subjects selected for tournament"""
        self.ngenes = ngenes
        self.nsubjects = nsubjects
        self.solution = solution
        self.mean_error = []
        self.min_error = []
        self.verbose = verbose
        self.n_tournament = n_tournament
        self.selection_method = selection_method

    def run(self):
        """Runs the Genetic Algorith (GA) from now on, and all its stages."""
        i = 1
        while True:
            if i % 500000 == 0:
                self.print_execution_info(i)
            i += 1
            self.evaluate_fitness()
            if self.verbose:
                self.mean_error.append(statistics.mean(self.error()))
                self.min_error.append(min(self.error()))
            if self.solution_found(): # When a solution is found it stops running
                break
            self.selection()
            self.reproduction()

    def print_execution_info(self, i):
        """Prints execution info"""
        error = self.mean_error_fitness() if i != 0 else 0
        print("Loop {}, Mean Fitness Error: {}".format(i, 0))
        print(self.subjects)

    def evaluate_fitness(self):
        """Calculates fitness for every object"""
        self.fitness = [self.fitness_f(s) for s in self.subjects]

    def selection(self):
        """Runs the method of selection given"""
        if self.selection_method == "roulette":
            self.selected = self.roulette()
        else:
            self.selected = self.tournament()

    def reproduction(self):
        """Does a crossover and then a mutation"""
        self.crossover()
        self.mutation()

    def roulette(self):
        """Implements roulette selection"""
        sum_sub = sum(self.fitness)
        for i, fit in enumerate(self.fitness):
            if sum_sub <= fit:
                return self.subjects[i-1]
            sum_sub -= fit

    def tournament(self):
        """Implements a tournament selection"""
        sel_index = random.sample(range(len(self.subjects)), self.n_tournament)
        max_fit = 0
        for i in sel_index:
            if self.fitness[i] > max_fit:
                max_fit = self.fitness[i]
                max_sub = self.subjects[i]
        return max_sub

    def solution_found(self):
        """If the value of the fitness of the solutions is in the fitnesses the solution is found"""
        if self.fitness_of_solution in self.fitness:
            return True
        return False


    def get_solution(self):
        """Returns the correct solution calculated"""
        for f, s in zip(self.fitness, self.subjects):
            if f == self.fitness_of_solution:
                return s

    def plot(self, n, algorithm):
        plt.figure(n)
        plt.title("Error vs Loop in {}".format(algorithm))
        plt.ylabel("Error")
        plt.xlabel("Loop")
        plt.plot(self.min_error, label="Minimum error of subjects")
        plt.plot(self.mean_error, label="Mean error of subjects")
        plt.legend()
        plt.savefig("{}.png".format(algorithm))


    @abstractmethod
    def fitness_f(self, subject):
        pass

    @abstractmethod
    def error(self):
        pass

    @abstractmethod
    def crossover(self):
        pass

    @abstractmethod
    def mutation(self):
        pass

def random_char():
    return random.choice(string.ascii_letters)

def random_string(n):
    return "".join([random_char() for i in range(n)])

class FindWordGA(GeneticAlgorithm):
    def __init__(self, ngenes, nsubjects, solution, selection_method="roulette", verbose=True, n_tournament = 8):
        """Calls super method and then creates random string of solution lengths as subjects"""
        super().__init__(ngenes, nsubjects, solution, selection_method, verbose, n_tournament)
        self.subjects = [random_string(ngenes) for i in range(nsubjects)]
        self.fitness_of_solution = ngenes # If all characters are equal solution is found
        if ngenes != len(solution):
            raise ValueError("Solution length has to be same of ngenes")
    

    def fitness_f(self, subject):
        """Returns the amount of chars equal to the solution"""
        fitness = 0
        for c1, c2 in zip(subject, self.solution):
            if c1 == c2: fitness += 1
        return fitness

    def error(self):
        """Return error as inverse of equal characters"""
        return [1.0 /(1+f)  for f in self.fitness]

    def crossover(self):
        """Standard crossover, mixes selected element with every subject on random index (one child per parents)"""
        i = random.randint(0, self.ngenes-1)
        sel1 = self.selected[:i]
        sel2 = self.selected[i:]
        new_subjects = []
        for s in self.subjects:
            if random.randint(0, 1) == 0:
                ns = s[:i] + sel2
            else:
                ns = sel1 + s[i:]
            new_subjects.append(ns)
        self.subjects = new_subjects
            
    def mutation(self):
        """Changes one char of every child randomly"""
        for i, s in enumerate(self.subjects):
            rchar = random_char()
            ri = random.randint(0, self.ngenes-1)
            self.subjects[i] = s[:ri] + rchar + s[ri+1:]

def random_bit():
    return random.choice("01")

def random_binary(n):
    return "".join([random_bit() for i in range(n)])

class DecimalToBinaryGA(GeneticAlgorithm):
    def __init__(self, ngenes, nsubjects, solution, selection_method="roulette", verbose=True, n_tournament = 8):
        """Calls super constructor and then creates random binary numbers as subjects"""
        super().__init__(ngenes, nsubjects, solution, selection_method, verbose, n_tournament)
        self.subjects = [random_binary(ngenes) for i in range(nsubjects)]
        self.fitness_of_solution = 0 # If difference between binary and solution is 0 solution is found
        if 2 ** ngenes < solution:
            raise ValueError("Genes have to be able to express number.")
    
    def fitness_f(self, subject):
        """Converts binary number to decimal and calculates difference with solution"""
        result = self.to_decimal(subject)
        return - abs(self.solution - result)

    def error(self):
        """The error is the difference between the solution and the result"""
        return [abs(f) for f in self.fitness]

    def to_decimal(self, binary):
        """Converts binary number to decimal"""
        result = 0
        exp = 1
        for c1 in binary:
            result += int(c1) * exp
            exp *= 2
        return result

    def crossover(self):
        """Standard crossover, mixes selected element with every subject on random index (one child per parents)"""
        i = random.randint(0, self.ngenes-1)
        sel1 = self.selected[:i]
        sel2 = self.selected[i:]
        new_subjects = []
        for s in self.subjects:
            if random.randint(0, 1) == 0:
                ns = s[:i] + sel2
            else:
                ns = sel1 + s[i:]
            new_subjects.append(ns)
        self.subjects = new_subjects
            
    def mutation(self):
        """Changes one random bit of each subject"""
        for i, s in enumerate(self.subjects):
            rchar = random_bit()
            ri = random.randint(0, self.ngenes-1)
            self.subjects[i] = s[:ri] + rchar + s[ri+1:]

def random_queen_positions(n):
    """Creates random valid (for row and column) queen position"""
    positions = list(range(n))
    random.shuffle(positions)
    return positions

class NQueensGA(GeneticAlgorithm):
    def __init__(self, ngenes, nsubjects, solution, selection_method="roulette", verbose=True, n_tournament = 8, prob_mutation = 0.1):
        super().__init__(ngenes, nsubjects, solution, selection_method, verbose, n_tournament)
        """Calls super method. Every queen position is given only by their column, the row is the index of the gene.
        This way it is known that the queens dont kill each other except on the diagonals. Gets a prob of having a 
        mutation."""
        self.subjects = [random_queen_positions(ngenes) for i in range(nsubjects)]
        self.prob_mutation = prob_mutation
        self.fitness_of_solution = 1 # If no queen was killed solution is found
        if ngenes != nsubjects:
            raise ValueError("Queens number has to be the same of gene number")
    

    def fitness_f(self, subject):
        """The inverse of the amount of queens that kill each other, can count if multiple queens each other multiple times.
        But the important thing is that when no queen is killed it returns zero, and that the worst cases return lowerfitness"""
        error = 0
        for row1, col1 in enumerate(subject):
            for row2, col2 in enumerate(subject):
                if (row1 != row2) and (abs(row2-row1) == abs(col1-col2)):
                    error += 1
        return 1.0 / (1 + error)
    
    def error(self):
        """The error is the inverse of the fitness"""
        return [1.0 / f for f in self.fitness]

    def crossover(self):
        """Keeps the values that the selected has equal to other subjects. Gets a random valid queen
        position for the other values"""
        new_subjects = []
        for s in self.subjects:
            positions = random_queen_positions(self.ngenes)
            index_equal = [i for i in range(self.ngenes) if s[i] == self.selected[i]]
            positions_equal = [s[i] for i in index_equal]
            positions = [p for p in positions if p not in positions_equal]
            child = []
            for i in range(self.ngenes):
                if i in index_equal:
                    child.append(s[i])
                else:
                    child.append(positions.pop())
            new_subjects.append(child)
        self.subjects = new_subjects

    def mutation(self):
        """With some low probability creates a completely random new position."""
        for i in range(self.nsubjects):
            if random.random() < self.prob_mutation:
                self.subjects[i] = random_queen_positions(self.ngenes)


def word_find_experiment():
    word_find = FindWordGA(3, 40, "cat")
    word_find.run()
    word_find.plot(1, "find_word_algorithm")
    print("WordFind solution: {}".format(word_find.get_solution()))


def decimal2bin_experiment():
    dec2bin = DecimalToBinaryGA(10, 40, 678)
    dec2bin.run()
    dec2bin.plot(2, "decimal2bin_algorithm")
    print("Decimal2Binary solution: {}".format(dec2bin.get_solution()))

def nqueens_experiment():
    nqueens = NQueensGA(8, 8, None, selection_method="tournament", n_tournament=2)
    nqueens.run()
    nqueens.plot(3, "nqueens_algorithm")
    print("NQueens solution: {}".format(nqueens.get_solution()))

word_find_experiment()
decimal2bin_experiment()
nqueens_experiment()
